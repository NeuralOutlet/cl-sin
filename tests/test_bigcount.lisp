
(load "../sin/sin_library")
(load "../sin/sin_stl_systems.lisp")

(defun equals-prec-two (value-a value-b)
	"checking against 2 decimal places for the equality of num"
	(let ((cvalue-a (* 100 value-a))
	      (cvalue-b (* 100 value-b)))

	(if (equal (round (imagpart cvalue-a)) (round (imagpart cvalue-b)))
		(if (equal (round (realpart cvalue-a)) (round (realpart cvalue-b)))
			t))))

(defun test (numsys)
	(let ((val "0"))
		
		(format t "[~a] 0~%" numsys)
		
		(loop for i from 0 below 10 do
			(progn
				(setq val (sin+ val "1"))
				(format t "[~a] ~a~%" numsys val)))
		
		(if (equals-prec-two (sin-truevalue val) 10)
			(format t "~%    PASSED    ~%~%")
			(format t "~%    FAILED    ~%~%"))))

(loop-active-systems
  (list 'binary 'ternary 'quarternary 'quinary 'senary 'septenary
        'octal 'nonary 'decimal 'undecimal 'duodecimal 'tridecimal
        'tetradecimal 'pentadecimal 'hexadecimal 'vigesimal 'sexagesimal
        'negabinary 'balanced 'dragon 'rus-binary 'rootbinary 'phinary
        'plasticbinary 'silverbase 'negarootbinary 'quaterimaginary)

  (test +activesystem+))

