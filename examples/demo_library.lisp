;; An example of the sin-<...> functions used to prob the types
;; these functions also work on strings that have a valid active system

(load "../sin/sin_library.lisp")
(load "../sin/sin_stl_systems.lisp")

;; define some generic numsys types
(defnumsys binary ()
	:structure (traverse +s+ sum term)
	:digits (gen-range 0 1 #'anu)
	:term (* (expt 2 +n+) +d+)
	
	:rules (generate-rules 2))

(defnumsys decimal ()
	:structure (traverse +s+ sum term)
	:digits (gen-range 0 9 #'anu)
	:term (* (expt 10 +n+) +d+))

(defnumsys roman ()
	:structure (traverse +s+ sum term)
	:digits '((#\I 1) (#\V 5) (#\X 10) (#\L 50) (#\C 100) (#\D 500) (#\M 1000))
	:term (+ +d+))

;; declare test to be a binary type
(defvar test (decnumsys binary))
(seta test "11111100101")

;; activate the three systems
(activate 'binary 'decimal 'roman)

(defmacro demo (numsys)
	`(progn
		(format t "~%Testing '~a'~%" ,numsys)
		(sin-description ,numsys)
		(format t "Rules: ~a~%" (sin-rules ,numsys))))

(demo test)
(demo "MMXXI")
(demo "2021")
(demo "fail")

(format t "~%Generate random numbers:~%")
(format t "  ~a~%" (gen-number "123" 5))
(format t "  ~a~%" (gen-number "123" 10))
(format t "  ~a~%" (gen-number "X" 5))
(format t "  ~a~%" (gen-number "123" 5 :unit #\1 :om #\0))
(format t "  ~a~%" (gen-number "123" 5 :unit #\1))
