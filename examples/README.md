# File descriptions #

The files prefixed with demo are to demonstrate basic use of a specific module of the sin codebase, demo_XXX relates directly to the library file sin_XXX.

The files prefixed with example are more complete programs exampling a task and using sin code to do so.

The smoke file runs all the demo files and is used as a quick check to make sure nothing major is broken.

# How to run them #

Replacing demo_arithmetic.lisp with which ever demo or example file you want to run:


```sh
    sbcl --script demo_arithmetic.lisp
```


On linux the smoke file can be run directly or as a commandline argument to SBCL


```sh
    ./smoke
    sbcl --script smoke
```