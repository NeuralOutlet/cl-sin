
;; We can create typed systems on the fly by combining (activate) and (seta)
;
;  (activate <list of system types>)
;
;  (seta <symbol name> <value> [optional: <system-type>])
;
;  'seta' will check the value against the types given to activate and will assign that type to the value

(load "../sin/sin_library.lisp")

;; Define a generalised positional numeral system
(defnumsys binary ()
	:structure (traverse +s+ sum term)
	:digits (gen-range 0 1 #'anu)
	:term (* (expt 2 +n+) +d+))


(defnumsys decimal ()
	:structure (traverse +s+ sum term)
	:digits (gen-range 0 9 #'anu)
	:term (* (expt 10 +n+) +d+))

(defnumsys roman ()
	:structure (traverse +s+ sum term)
	:digits '((#\I 1) (#\V 5) (#\X 10) (#\L 50) (#\C 100) (#\D 500) (#\M 1000))
	:term (+ +d+))


;; ----------------------- ;;

(defun demo (value)
	(format t "~a in ~a is ~a~%"
	        (numsys-value-string value)
	        (numsys-value-system value)
	        (sin-truevalue value)))

;; static declaration of type
(defvar aye (decnumsys binary))
(seta aye "10001")
(demo aye)

;; editing a statically typed value and
;; failing because it is the wrong type
(defvar bee aye)
(seta bee "123")
(demo bee)

;; activating three systems
(activate 'binary 'decimal 'roman)

;; dynamically using the active systems
(defvar sea nil)
(seta sea "1231")
(demo sea)


