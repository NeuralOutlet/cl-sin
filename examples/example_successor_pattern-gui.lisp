
;; start Quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
	(when (probe-file quicklisp-init)
		(load quicklisp-init)))

;; Load Common Qt
(ql:quickload 'qt)

;; Define package
(defpackage :successor-patterns
	(:use :cl :qt :common-lisp)
	(:export #:main))
(in-package :successor-patterns)
(named-readtables:in-readtable :qt)

;; load in SIN
(load "../sin/sin_library.lisp")
(load "../sin/sin_stl_systems.lisp")

(defvar *grid-size* 512)
(defvar *tile-size* 10)
(defvar *tile-count* *grid-size*)


(defun append-coord (coords cvalue)
	(append coords (list (list (realpart cvalue) (imagpart cvalue)))))

;; A function for generating a list of (x, y) co-ordinates. This is done
;; by performing the successor function and then casting the output to base -1+i.
(defun gen-successor-pattern (amount outsystem)
	(let ((value "0") (cvalue #c(0 0)) (output (list (list 0 0))))
		(loop for i from 0 to amount do
			(progn
				(setq value (sin+ value "1"))
				(setq cvalue (sin-cast value outsystem))
				(setq output (append-coord output cvalue))))
		output))

;;numeral systems with metallic means as the radix
(defnumsys metallic-1 (positional-real) :r (/r (+r 1 (sqrt-r 5)) 2))
(defnumsys metallic-2 (positional-real) :r (/r (+r 2 (sqrt-r 8)) 2))
(defnumsys metallic-3 (positional-real) :r (/r (+r 3 (sqrt-r 13)) 2))
(defnumsys metallic-4 (positional-real) :r (/r (+r 4 (sqrt-r 20)) 2))
(defnumsys metallic-5 (positional-real) :r (/r (+r 5 (sqrt-r 29)) 2))

;; gaussian integer complex numeral systems for projectio onto the 2D plane
(defnumsys compgauss-1 (positional) :r #c(-1 1))
(defnumsys compgauss-2 (positional) :r #c(-2 1))
(defnumsys compgauss-3 (positional) :r #c(-3 1))
(defnumsys compgauss-4 (positional) :r #c(-4 1))
(defnumsys compgauss-5 (positional) :r #c(-5 1))


;; here we generate a black bacground with a simple grid on it then fill it
;; with tiles based on positions obtained from the successor pattern function
(defun generate-pattern-image (filename outsystem)
	(let ((img (#_new QImage *grid-size* *grid-size* (#_QImage::Format_ARGB32)))
	      (fill-colour (#_rgb (#_QColor::fromRgb 240 240 240)))
	      (grid-colour (#_rgb (#_QColor::fromRgb 150 150 150))))

		(#_fill img fill-colour)

		;; draw a grid overlay
		(loop for y from 0 below *grid-size* do
			(loop for x from 0 below *grid-size*
			  when (or (equal (mod x 10) 0) (equal (mod y 10) 0)) do
				(#_setPixel img x y grid-colour)))
		; add a central pixel line on x and y axis:
		;	  when (or (equal x (1- (/ *grid-size* 2))) (equal y (1- (/ *grid-size* 2)))) do
		;		(#_setPixel img x y 0)))

		;; draw on the tiles
		(loop for coord in (gen-successor-pattern (- *tile-count* 2) outsystem)
		      for i from 0 do
			(draw-tile img coord i))

		;; centre point if desired
		;(draw-tile img (list 0 0) 1)
	
		;; save the output
		(#_save img filename "JPG" 100)))


;; If the tiles are everto overlap the colour should change so it is noticable
(defun overlap-colour (img x y)
	(let* ((qcol (#_pixel img x y))
	       (col (#_QColor::fromRgb qcol)))
	(cond
		((equal (#_red col) 240) (#_rgb (#_QColor::fromRgb 255 0 0)))
		((equal (#_red col) 255) (#_rgb (#_QColor::fromRgb 255 255 0)))
		((equal (#_green col) 255) (#_rgb (#_QColor::fromRgb 255 255 255)))
		(t (#_rgb (#_QColor::fromRgb 255 255 255))))))

(defun range-colour (v vmin vmax)
	(let ((col (#_QColor::fromRgb 255 255 255))
	      (dv (- vmax vmin)))

	(if (< v vmin)
		(setq v vmin))

	(if (> v vmax)
		(setq v vmax))
	
	(cond
		((< v (+ vmin (* 0.25 dv)))
			(setq col (#_QColor::fromRgb
				0
				(round (* (/ (* 4 (- v vmin)) dv) 255))
				255)))
		((< v (+ vmin (* 0.5 dv)))
			(setq col (#_QColor::fromRgb
				0
				(round (* (+ 1 (/ (* (- (+ (* 0.25 dv) vmin) v) 4) dv)) 255))
				255)))
		((< v (+ vmin (* 0.75 dv)))
			(setq col (#_QColor::fromRgb
				(round (* (/ (* 4 (- v vmin (* 0.5 dv))) dv) 255))
				255
				0)))
		(t
			(setq col (#_QColor::fromRgb
				255
				(round (* (+ 1 (/ (* (- (+ (* 0.75 dv) vmin) v) 4) dv)) 255))
				0))))

		(#_rgb col)))


;; draw a N by N pixel square centred on the image in the coord position
(defun draw-tile (img coord index)
	(let* ((scaled-coord (list
	                     	(round (* (car coord) *tile-size*))
	                     	(round (* (cadr coord) *tile-size*))))
	       (xstart (+ (- (car scaled-coord) (/ *tile-size* 2)) (/ *grid-size* 2)))
	       (ystart (+ (- (cadr scaled-coord) (/ *tile-size* 2)) (/ *grid-size* 2))))

		(loop for y from ystart below (+ ystart *tile-size*) do
			(loop for x from xstart below (+ xstart *tile-size*)
			  when (and (< x *grid-size*) (< y *grid-size*) (>= x 0) (>= y 0)) do
				(#_setPixel img x y (overlap-colour img x y)))))) ; (range-colour index 0 *tile-count*))))))


;;; Main program
(let* ((app (make-qapplication))
       (gridsize (write-to-string *grid-size*))
       (tilecount (write-to-string *tile-count*))
       (filename ""))

	(loop-active-systems-multi
	 (list
	  (list 'binary 'compgauss-1)
	  (list 'metallic-1 'compgauss-1)
	  (list 'ternary 'compgauss-2)
	  (list 'metallic-2 'compgauss-2)
	  (list 'quarternary 'compgauss-3)
	  (list 'metallic-3 'compgauss-3)
	  (list 'quinary 'compgauss-4)
	  (list 'metallic-4 'compgauss-4))

	(format t "Generating successor pattern for ~a in ~a space...~%"
		(string (car +activesystems+)) (string (cadr +activesystems+)))

	(setq filename (concatenate 'string gridsize "-grid-" (string (car +activesystems+)) "-" tilecount "-increments.jpg"))
	(generate-pattern-image filename (cadr +activesystems+))))


