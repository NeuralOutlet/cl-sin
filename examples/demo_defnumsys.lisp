
(load "../sin/sin_library.lisp")

; ---------------
; system examples
; ---------------

;; Define a generalised positional numeral system
(defnumsys positional ()
	:structure (sum-series +s+ term)
	:digits (radix-alphabet r)
	:term (* (expt r n) +d+)
	:r 0)

;; Define a generalised positional numeral system for
;; irrational numbers that uses the computational-reals
(defnumsys positional-real ()
	:structure (sum-series-real +s+ term)
	:digits (radix-alphabet r)
	:term (*r (expt-r r n) +d+)
	:r 0)

;; Inherit from the above system and define the radix
(defnumsys hexadecimal (positional) :r 16)
(defnumsys decimal (positional) :r 10)
(defnumsys binary (positional) :r 2)

;; Chain the inheritance
(defnumsys negabinary (binary) :r -2)

;; Inherit but redefine one of the core properties
(defnumsys min-binary (binary)
	:digits '((#\1 1) (#\2 2)))

(defnumsys balanced (positional)
	:digits '((#\T -1) (#\0 0) (#\1 1)))

;; Complex numeral system via digit values
(defnumsys abcd (negabinary)
	:digits '((#\0 0) (#\A 1) (#\B -1) (#\C #c(0 1)) (#\D #c(1 1))))

;; Complex numeral system via radix value
(defnumsys dragon (positional) :r #c(-1 1))

;; Non-integer radix systems
(defnumsys rootbinary (positional-real) :r (sqrt-r 2))
(defnumsys phinary (positional-real) :r (/r (+r 1 (sqrt-r 5)) 2))
(defnumsys rus-binary (positional) :r (/ (+ 1 (sqrt -7)) 2))

;; Here is a non-positional system for comparison
(defnumsys roman ()
	:structure (loop for d across +s+ sum term)
	:digits '((#\I 1) (#\V 5) (#\X 10) (#\L 50) (#\C 100) (#\D 500) (#\M 1000))
	:term +d+)


; --- demo examples --- ;
(defmacro test (type val)
	`(let* ((valid (if (typep ,val (quote ,type)) "Yes." "No.")) (spaces "")
	       (outstr (format nil "~%Is ~a valid in ~a? ~a" ,val (quote ,type) valid)))
		(loop repeat (- 55 (length outstr)) do
			(setq spaces (concatenate 'string spaces " ")))
		(format t "~a" outstr)
		(if (typep ,val (quote ,type))
			(let* ((temp (,type ,val))
			       (result (if (rationalp temp) (float temp) temp)))
				(format t "~a '~a' = ~a" spaces ,val result)))))

(format t "~%Examples of Usual Integer systems~%")
(test binary "1101")
(test decimal "13")
(test hexadecimal "D")

(format t "~%~%Examples of Unusual Integer systems~%")
(test min-binary "221")
(test negabinary "11101")
(test balanced "111")
(test balanced "TTT")
(test roman "XIII")

(format t "~%~%Examples of Complex Integer systems~%")

(test dragon "100010001")
(test dragon "1100110011")
(test abcd "AAA0A")
(test abcd "CCC0C")

(format t "~%~%Examples of radix-expansion~%")
(test binary ".")
(test decimal "12.345")
(test binary "1100.01011000010100011111")
(test hexadecimal "C.5851EB851EB851EB851F")

(format t "~%~%Examples of Non-integer systems~%")
(test rootbinary "1010001")
(test phinary "100010.001001")
(test rus-binary "101100011101")

(format t "~%~%Examples of illegal words~%")
(test positional "1101")
(test binary "127.0.0.1")
(test decimal "FAIL")
(test roman "X.X")
(format t "~%~%")


