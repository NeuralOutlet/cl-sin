
(load "../sin/sin_library.lisp")
(load "../sin/sin_stl_systems.lisp")

(setq *print-prec* 10)

(defun demo (numsys radix)
	(format t "------------------------------~%")

	(format t "~%Rulesets for ~a~%~%" radix)
	(loop for ruleset in (sin-rules numsys) do
		(format t "~a~a~%" #\tab ruleset))

	;; A spread of the first 10 integers
	(format t "~%Greedy construction from 1 to 10~%")
	(format t "1:  ~a~%" (logmax-builder 1 radix))
	(format t "2:  ~a~%" (logmax-builder 2 radix))
	(format t "3:  ~a~%" (logmax-builder 3 radix))
	(format t "4:  ~a~%" (logmax-builder 4 radix))
	(format t "5:  ~a~%" (logmax-builder 5 radix))
	(format t "6:  ~a~%" (logmax-builder 6 radix))
	(format t "7:  ~a~%" (logmax-builder 7 radix))
	(format t "8:  ~a~%" (logmax-builder 8 radix))
	(format t "9:  ~a~%" (logmax-builder 9 radix))
	(format t "10: ~a~%" (logmax-builder 10 radix)))

(demo 'binary 2)
(demo 'phinary (/r (+r 1 (sqrt-r 5)) 2))
(demo 'rootbinary (sqrt-r 2))
(demo 'silverbase (+r 1 (sqrt-r 2)))
(demo 'plasticbinary plastic)
