
(load "../sin/sin_library.lisp")
(load "../sin/sin_stl_systems.lisp")

(loop-active-systems (list 'binary 'phinary 'decimal)
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1" "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1" "1" "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1" "1" "1" "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1" "1" "1" "1" "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1" "1" "1" "1" "1" "1" "1"))
	(format t "[~a] ~a~%" +activesystem+ (sin+ "1" "1" "1" "1" "1" "1" "1" "1" "1" "1")))

