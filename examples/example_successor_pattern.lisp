
(load "../sin/sin_library.lisp")
(load "../sin/sin_stl_systems.lisp")

(defun append-coord (coords cvalue)
	(append coords (list (list (realpart cvalue) (imagpart cvalue)))))

;; A function for generating a list of (x, y) co-ordinates. This is done
;; by performing the successor function and then casting the output to base -1+i.
(defun gen-successor-pattern (amount outsystem)
	(let ((value "0") (cvalue #c(0 0)) (output (list (list 0 0))))
		(loop for i from 0 to amount do
			(progn
			    (format t "~a: ~a --> ~a~%" i value (sin-cast value outsystem))
				(setq value (sin+ value "1"))
				(setq cvalue (sin-cast value outsystem))
				(setq output (append-coord output cvalue))))
		output))

;;numeral systems with metallic means as the radix
(defnumsys metallic-1 (positional-real) :r (/r (+r 1 (sqrt-r 5)) 2))
(defnumsys metallic-2 (positional-real) :r (/r (+r 2 (sqrt-r 8)) 2))
(defnumsys metallic-3 (positional-real) :r (/r (+r 3 (sqrt-r 13)) 2))
(defnumsys metallic-4 (positional-real) :r (/r (+r 4 (sqrt-r 20)) 2))
(defnumsys metallic-5 (positional-real) :r (/r (+r 5 (sqrt-r 29)) 2))

;; gaussian integer complex numeral systems for projectio onto the 2D plane
(defnumsys compgauss-1 (positional) :r #c(-1 1))
(defnumsys compgauss-2 (positional) :r #c(-2 1))
(defnumsys compgauss-3 (positional) :r #c(-3 1))
(defnumsys compgauss-4 (positional) :r #c(-4 1))
(defnumsys compgauss-5 (positional) :r #c(-5 1))

(loop-active-systems-multi
  (list
    (list 'metallic-1 'compgauss-1)
    (list 'metallic-2 'compgauss-2)
    (list 'metallic-3 'compgauss-3)
    (list 'metallic-4 'compgauss-4)
    (list 'metallic-5 'compgauss-5))

	(format t "~%~a~%~%" (gen-successor-pattern 10 (cadr +activesystems+))))
