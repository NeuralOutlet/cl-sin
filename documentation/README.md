# Documentation Syntax #

The documentation here takes after the Common Lisp HyperSpec while reducing the amount of descriptive flexability to make it somewhat more accessable. Of the below list each function or variable being defined will have at least 1,2, and 5:

1. Identifier
2. Template parameters
3. Expression list
4. Parameter description
5. General description


## Template Syntax ##

As the HyperSpec uses a modified Backus–Naur form, here we use an even smaller subset:

```
A ==> B          indicates the evaluation of expression A returns expression B.
(A [[ B ]] C)    indicates B is to be spliced in, such that we get (A B C) or (A C).
(A [[ B+ ]] C)   indicates at least one B is to be spliced in, but there can be more, such that (A C) is not valid but (A B C), (A B B C), (A B B B C), ... all are.
(A [[ B* ]] C)   indicates the same as above except there can be zero instances of B aswell.
(A [[ B C ]])    indicates that B and C can be spliced in, in any order, such that (A B C), (A C B), (A B), (A C), and (A) are all valid.
```


**name**         denotes a symbol already defined in the common lisp package.


name             denotes an expression that is to be defined in the expression list.


*name*           denotes a parameter value that is to be described in the parameter decription.


# Library Reference #

- - - - -

## DEFNUMSYS ##

(**defnumsys** *systemname* (*supertype*\*) [[ keypair\* ]]) ==> *result*

keypair ::= :key *keyvalue*


Parameters:

* systemname - the name of the system to be defined
* supertype  - an already defined numsys
* keyvalue - the value for a key 
* result - the result is a deftype function call that defines the type predicate for the system


Description:

For a numsys to be valid it requires three specific keypairs for the keys 'structure', 'term', and 'digits'.

- - - - -

## DECNUMSYS ##



- - - - -