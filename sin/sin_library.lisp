;; load initial requirement of coputtional-reals ;;
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload :computable-reals)
(use-package :computable-reals)
(defvar *sin-comp-reals-loaded* t)
(setf *read-default-float-format* 'double-float)

;; load SIN code
(load (merge-pathnames "sin_defnumsys.lisp" *load-truename*))
(load (merge-pathnames "sin_inductive_rules.lisp" *load-truename*))
(load (merge-pathnames "sin_activate_seta.lisp" *load-truename*))
(load (merge-pathnames "sin_arithmetic.lisp" *load-truename*))

; ---------------------------------- ;
; General Standard Library Functions ;
; ---------------------------------- ;

; functions directly related to numeral systems

(defun sin-type (numsys)
	"returns the type of the numeral system"
	(if (symbolp numsys) ; pass straight through if symbol given
		numsys
		(if (typep numsys 'numsys)
			(numsys-value-system numsys)
			(match-system numsys nil))))


(defun sin-rawstring (numsys)
	"If the numsys is a system this returns the string. If this is a string it checks for a valid active system then returns the string (or nil if no active system matches the string)"
	(if (typep numsys 'numsys)
		(numsys-value-string numsys)
		(if (match-system numsys nil)
			numsys
			nil)))

(defun sin-truevalue (numsys)
	"evaluate the rawstring via the numeral system rules"
	(let ((systype (sin-type numsys)))
		(if (not (null systype))
			(funcall systype (sin-rawstring numsys)))))

(defun sin-cast (value numsys)
	"if value is a valid numsys then it returns the evaluation of that number interpreted as if it were numsys"
	(if (funcall (add-to-symbol numsys "P") value)
		(funcall numsys value)))

(defun sin-numeral (numeral numsys)
	"get corresponding digit value or digit symbol (depending on input type)."
	(let ((num-or-char (if (stringp numeral) (char numeral 0) numeral)))
		(funcall (add-to-symbol numsys "-NUMERAL") num-or-char)))

(defun sin-alphabet (numsys &key (digits-only nil))
	"Returns a list of pairs in the form (digit digit-value). using ':digits-only t' will strip any symbols that have a nil value."
	(let ((systype (sin-type numsys)))
		(if (not (null systype))
			(let ((alpha (funcall (add-to-symbol systype "-ALPHABET"))))
				(if digits-only
					(loop for a in alpha when (not (null (nth 1 a))) collect a)
					alpha)))))

;; converts string numsys to list of numerical values
(defun sin-value-list (value)
	(let ((numsys (sin-type value)))
		(loop for ch across (sin-rawstring value)
		  when (alphanumericp ch) collect
			(sin-numeral ch numsys))))

(defun sin-rules (numsys &key (pick 'all))
	"Returns a list of pairs in the form (symbol <list of rules>). :pick <symbol> will return only rules for that."
	(let ((systype (sin-type numsys)))
		(if (not (null systype))
			(let ((rules (funcall (add-to-symbol systype "-RULES"))))
				(case pick
					((nil) (cadr (assoc nil rules))) ;; standardisation
					('+ (cadr (assoc '+ rules)))     ;; normalisation
					('- (cadr (assoc '- rules)))
					('* (cadr (assoc '* rules)))
					('/ (cadr (assoc '/ rules)))
					(otherwise rules))))))

(defun sin-description (numsys)
	"Function to print (in pretty format) the number system details"
	(let ((rawstr (sin-rawstring numsys))
	      (truevalue (sin-truevalue numsys))
	      (alphabet (loop for pair in (sin-alphabet numsys) collect (car pair))))
	(format t "  Value: ~a = ~a~%  System: ~a ~a~%"
	          rawstr truevalue (sin-type numsys) alphabet)))


; --- value mapping functions --- ;

(defun anu (number) ; (a)lpha(n)umeric (u)ppercase
	(if (> number 35)
		#\?
		(if (> number 9)
			(code-char (+ (- number 9) 64))
			(code-char (+ number 48)))))

(defun anl (number) ; (a)lpha(n)umeric (l)owercase
	(if (> number 35)
		#\?
		(if (> number 9)
			(code-char (+ (- number 9) 96))
			(code-char (+ number 48)))))


; --- Generative functions --- ;

(defun gen-number (numsys length &key (unit nil) (om nil))
	"Generates a string of size length using alphabet values. a single unit value can be generated when used alongside an 'om' value (filler for all other positions)"
	(let ((alphabet (loop for pair in (sin-alphabet numsys :digits-only t) collect (car pair)))
	      (unit (if (stringp unit) (char unit 0) unit))
	      (om (if (stringp om) (char om 0) om))
	      (output ""))

		(if (and unit (member unit alphabet))
			(let ((rand (random length)))

				(if (not (member om alphabet))
					(setq om (car alphabet)))

				(loop for i from 0 below length do
					(if (equal i rand)
						(setq output (concatenate 'string output (string unit)))
						(setq output (concatenate 'string output (string om))))))

			(loop for i from 0 below length do
				(let ((randch (nth (random length) alphabet)))
					(setq output (concatenate 'string output (string randch))))))
		output))

(defun gen-range (start finish digit-func)
	(loop for n from start to finish
		collect (list (funcall digit-func n) n)))


; --- helper functions for the usual numsys suspects --- ;

(defmacro traverse (str action body)
	`(loop for +n+ from (1- (length ,str)) downto 0 ,action
		(let ((d (char ,str (- (length ,str) +n+ 1))))
			,body)))

(defmacro sum-series (str body)
	`(let* ((test (position #\. ,str))
	        (terms (if test (remove #\. ,str) ,str))
	        (offset (if test (- (length terms) test) 0)))
		(loop for +n+ from (1- (length terms)) downto 0 sum
			(let ((d (char terms (- (length terms) +n+ 1)))
			      (n (- +n+ offset)))
				,body))))

(defmacro sum-series-real (str body)
	`(let* ((test (position #\. ,str))
	        (terms (if test (remove #\. ,str) ,str))
	        (offset (if test (- (length terms) test) 0))
			(value 0))
		(loop for +n+ from (1- (length terms)) downto 0 do
			(setq value 
				(+r value
				(let ((d (char terms (- (length terms) +n+ 1)))
			      (n (- +n+ offset)))
				,body))))
		(float-r value)))

(defun absolute (value)
	(if (complexp value)
		(abs value)
		(abs (float-r value))))

; this is our function for defining the alphabet
; used by some radix in a positional system
  ; if complex return self else return float-r self
(defun radix-alphabet (radix)
	(let ((active (gen-range 0 (1- (ceiling (absolute radix))) #'anu))
	      (passive #2<(#\. nil))) ; radix point can only appear < 2 times
		(cons passive active)))

; --- used in latest version of computational-reals --- ;

(defun rational-approx-r (x k)
	"Produce a rational approximation of X called R such that
	 |R - X| < 2^(-K)."
	(/ (approx-r x k) (expt 2 k)))

(defun float-r (value &optional (prec 200))
	(if (floatp value)
		value
		(float (rational-approx-r value prec))))
