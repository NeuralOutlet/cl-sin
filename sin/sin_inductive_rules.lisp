
;; why doesn't this exist
(defun strapp (str1 str2)
	(concatenate 'string str1 str2))

;; take a radix value
;(defvar *radix* (/r (+r 1 (sqrt-r 5)) 2)) ; phi
(defvar *precision* 50) ; assume infinite expansion after 100 digits

; just getting a usable value out of a creal
(defun creal-value (crv)
	(let* ((str (format nil "~a" crv))
		   (start (position #\+ str))
		   (end (search "..." str))
		   (nega (position #\- str)))

		(read-from-string
			(if nega
				(subseq str nega end)
				(if (null start)
					str
					(subseq str (1+ start) end))))))


;; create a string via logmax greedy method:
(defun logmax-builder (value radix)
	(let* ((msd (/r (log-r value 10) (log-r radix 10)))
	       (safe-to-stop nil)
	       (numstr "")
	       (dcount 0)
	       (write-digit nil))

		(setq msd (floor-r msd))
		(loop until (or (and (<= (creal-value value) 0) safe-to-stop) (< msd (- *precision*)) (equal radix 0)) do
			(let ((next (-r value (expt-r radix msd))))

				(setq write-digit nil)

				(if (and (>= (creal-value next) 0) (< (1+ dcount) (creal-value radix)))
					(progn
						(setq value next)
						(setq dcount (1+ dcount)))
					(setq write-digit t))

				(if write-digit
					(progn
						(setq numstr (strapp numstr (write-to-string dcount)))
						(setq dcount 0)
						(setq msd (-r msd 1))))

				(if (= (creal-value msd) -1)
					(progn
						(if (not safe-to-stop)
							(setq numstr (strapp numstr ".")))
						(setq safe-to-stop t)))))
		(if (> dcount 0)
			(setq numstr (strapp numstr (write-to-string dcount))))
		numstr))


;; ---------- generate a unit list of a value using greedy build ----------- ;;

(defun case-subs (value radix)
	(if (> (creal-value radix) 0)
		(let* ((msd (/r (log-r value 10) (log-r radix 10)))
		       (dcount 0)
		       (write-digit nil)
		       (output nil))
			(setq msd (floor-r msd))
			(loop until (and (< msd 0) (or (<= (creal-value value) 0) (< msd (- *precision*)) (equal radix 0))) do
				(let ((next (-r value (expt-r radix msd))))
					(setq write-digit nil)
					(if (and (>= (creal-value next) 0) (< (1+ dcount) (creal-value radix)))
						(progn
							(setq value next)
							(setq dcount (1+ dcount)))
						(setq write-digit t))
					(if write-digit
						(progn
							(setq output (append output (list dcount)))
							(setq dcount 0)
							(setq msd (-r msd 1))))))
		(if (> dcount 0)
				(setq output (append output (list dcount))))
			output)))

;; ----------- forward facing function for users ---------- ;;

(defun generate-standardise-rules (radix)
	(let* ((msd (*r -1 1))
	       (value (*r 1 1))
	       (dcount 0)
	       (write-digit nil)
	       (kernel-mask '(0))
	       (kernel-adder '(1)))

		(setq msd (floor-r msd))
		(loop until (or (<= (creal-value value) 0) (< msd (- *precision*)) (equal radix 0)) do
			(let ((next (-r value (expt-r radix msd))))

				(setq write-digit nil)

				(if (and (>= (creal-value next) 0) (< (1+ dcount) (creal-value radix)))
					(progn
						(setq value next)
						(setq dcount (1+ dcount)))
					(setq write-digit t))

				(if write-digit
					(progn
						(setq kernel-mask (append kernel-mask (list dcount)))
						(setq kernel-adder (append kernel-adder (list (- dcount))))
						(setq dcount 0)
						(setq msd (-r msd 1))))))

		(if (> dcount 0)
			(progn
				(setq kernel-mask (append kernel-mask (list dcount)))
				(setq kernel-adder (append kernel-adder (list (- dcount))))))

		(if (> (length kernel-mask) *precision*)
			(list (list 0) (list 0))
			(list kernel-mask kernel-adder))))


(defun generate-addition-rules (radix)
	(let* ((max-value (ceiling (abs (creal-value radix))))
	       (inductive-case (case-subs max-value radix))
	       (offset 0))
	
		(if (> (creal-value radix) 0)
			(setq offset (- (floor-r (/r (log-r max-value 10) (log-r radix 10))))))

		(loop for val in inductive-case collect
			(let ((pair (list offset (if (equal offset 0) (- max-value) val))))
				(setq offset (1+ offset))
				pair))))


(defun generate-rules (radix)
	(list
		(list nil (generate-standardise-rules radix))
		(list '+ (generate-addition-rules radix))))


