
(defpackage :sin
	(:use :common-lisp)
	(:intern :parse-key-list :type-sum :illegal-count)
	(:export :defnumsys))
