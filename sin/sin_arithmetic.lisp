

;; the default function for transforming  digits into the correct form
(defun formalise (value overflow-trigger &key numsys)
	(let ((input (copy-list value)))

		(setq value (normalise-form value overflow-trigger :numsys numsys))
		(setq value (standardise-form value :numsys numsys))

		(if (equal input value)
			value
			(formalise value overflow-trigger :numsys numsys))))

;; before this point the unit list is generated using the alphabet values and an offset is recorded
(defun normalise-form (value overflow-trigger &key numsys)
	"Takes a value in the form of a unit list and its system type, performs that system's normalisation rule (overflow)"
	(let ((ruleset (sin-rules numsys :pick '+))
	      (in-value (copy-list value)))
		
		(loop for index from (1- (length value)) downto 0 do
			(if (>= (nth index value) overflow-trigger)
				(apply-rules ruleset index value)))

		;; repeat until normalising changes nothing
		(if (equal in-value value)
			value
			(normalise-form value overflow-trigger :numsys numsys))))


(defun apply-rules (ruleset index genome)
	(loop for rule in ruleset do
		(let ((offset (+ index (nth 0 rule))))
			(if (and (>= offset 0) (< offset (length genome)))
				(setf (nth offset genome) (+ (nth offset genome) (nth 1 rule))))))
	(1- (length genome))) ; return reset value for normalise-form loop

;; check that the kernel values are all >= the substring values
(defun kernel-gt (genome kernel start)
	(let ((result t))
		(loop for i from 0 to (1- (length kernel)) do
			(if (< (nth (+ i start) genome) (nth i kernel))
				(setq result nil)))
		result))

(defun kernel-add (genome kernel start)
	(let ((output genome))
		(loop for k from 0 to (1- (length kernel)) do
			(let ((i (+ start k)))
				(setf (nth i output) (+ (nth i output) (nth k kernel)))))
		output))

;ruleset is a pair of N-length lists
; the first list is a matching kernel: substr > kernel
; the second list is an addition mask: substr + mask
;for phinary it is:  '((0 1 1) (1 -1 -1))
(defun standardise-form (value &key numsys)
	(let* ((ruleset (sin-rules numsys :pick nil))
	       (kernel (nth 0 ruleset))
	       (mask (nth 1 ruleset))
	       (input (copy-list value))
	       (output value))

		(loop for i from 0 to (- (length value) (length kernel)) do
			(let ((pos (- (length value) i (length kernel))))
				(if (kernel-gt output kernel pos)
					(setq output (kernel-add output mask pos)))))
		(if (equal input output)
			output
			(standardise-form value :numsys numsys))))


;; function to stack unit values such that
;  (list-unit-add '(1 0 1) '(1 0 1))
; ==> '(2 0 2)
(defun list-unit-add (&rest lsts)
	(loop for i from 0 to (1- (length (car lsts))) collect
		(loop for lst in lsts sum (nth i lst))))


;; a concatenation fnction that maps the alphabet numeral back in
(defun write-numsys (str digit numsys &key (lhs nil))
	(let ((ch (sin-numeral digit numsys)))

		(if lhs
			(concatenate 'string str (string ch))
			(concatenate 'string (string ch) str))))


; create a string with radix point from a list of digits and an offset
(defun format-num (digits radix-offset numsys &key (fixed-padding nil))
	(let ((output "") (rhs "") (padding (not fixed-padding)))

		; LHS of the number
		(loop for i from 0 below radix-offset do
			(let ((digit (nth i digits)))
				(if (not (equal digit 0))
					(setq padding nil))
				(if (and (not padding) (not (null digit)))
					(setq output (write-numsys output digit numsys :lhs t)))))

		; RHS of the number
		(setq padding (not fixed-padding))
		(loop for i from 0 below (- (length digits) radix-offset) do
			(let ((digit (nth (- (length digits) i 1) digits)))
				(if (not (equal digit 0))
					(setq padding nil))
				(if (not padding)
					(setq rhs (write-numsys rhs digit numsys :lhs nil)))))
		
		(if (> (length rhs) 0)
			(concatenate 'string output "." rhs)
			output)))


(defun same-sin-type-p (args)
	(let ((numsys (sin-type (car args)))
	      (operands (cdr args)))
		(loop for num in operands never
			(not (equal numsys (sin-type num))))))

;; gets position of radix
(defun radix-offset (value)
	(let ((radixpos (position #\. value)))
		(if (null radixpos)
			(length value)
			radixpos)))

(defun expansion-length (value)
	(let ((len (- (length value) (radix-offset value))))
		(if (position #\. value)
			(1- len)
			len)))

(defun strapp (str1 str2)
	(concatenate 'string str1 str2))

(defun append-char-n (str ch n &key (rhs nil))
	(if (equal n 0)
		str
		(if rhs
			(append-char-n (strapp str (string ch)) ch (1- n) :rhs t)
			(append-char-n (strapp (string ch) str) ch (1- n)))))

;; takes numbers as strings and returns the same numbers padded with 0s
(defvar *sin-padding* 10)
(defun align-values (args &optional (padding *sin-padding*))
	(let ((maxlhs 0) (maxrhs 0)
	      (paddedvalues '()))
		(loop for value in args do
			(let ((lhslen (radix-offset value))
			      (rhslen (expansion-length value)))
				(if (< maxlhs lhslen) (setq maxlhs lhslen))
				(if (< maxrhs rhslen) (setq maxrhs rhslen))))

		(loop for value in args collect
			(let* ((lhslen (radix-offset value))
			       (rhslen (expansion-length value))
			       (val (if (equal rhslen 0) (strapp value ".") value)))
				(append-char-n
					(append-char-n
						val
						#\0
						(+ (- maxlhs lhslen) padding))
					#\0
					(+ (- maxrhs rhslen) padding) :rhs t)))))

(defun sin+ (&rest unaligned-args)
	(if (same-sin-type-p unaligned-args)
		(let* ((args (align-values unaligned-args))
		       (numsys (sin-type (car args)))
		       (list-args (loop for arg in args collect (sin-value-list arg)))
		       (sumvalue (apply #'list-unit-add list-args))
		       (overflow-trigger (1+ (loop for unit in
		                             	(sin-alphabet numsys :digits-only t)
		                             		maximize (cadr unit))))
		       (sumvalue (formalise sumvalue overflow-trigger :numsys numsys)))
		(format-num sumvalue (radix-offset (car args)) numsys))))

;; was previously used in (sin-fixed+ ...)
;	    	(if (and (same-sin-type-p args)
;	    	     (check-fixed-size (car args) (cdr args))))
;; but no longer used to save on computation time
(defun check-fixed-size (value args)
	(let ((vlen (length value))
	      (vroff (radix-offset value)))
	(loop for arg in args always
		(and (equal vlen (length arg))
		     (equal vroff (radix-offset arg))))))

;; a fixed size version of (sin+ ...)
(defun sin-fixed+ (&rest args)
	(let* ((numsys (sin-type (car args)))
	       (list-args (loop for arg in args collect (sin-value-list arg)))
	       (sumvalue (apply #'list-unit-add list-args))
	       (overflow-trigger (1+ (loop for unit in
	                             	(sin-alphabet numsys :digits-only t)
	                             		maximize (cadr unit))))
	       (sumvalue (formalise sumvalue overflow-trigger :numsys numsys)))
	(format-num sumvalue (radix-offset (car args)) numsys :fixed-padding t)))

