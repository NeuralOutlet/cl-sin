
;; We fully define 'base 0' such that other numsys definitions
;; can inherit from it, only requiring the radix to be redefined
(defnumsys positional ()
	:structure (sum-series +s+ term)
	:digits (radix-alphabet r)
	:term (* (expt r n) +d+)
	:r 0
	
	:rules (generate-rules r))

;; Notable integer numeral systems:
(defnumsys binary (positional) :r 2)
(defnumsys ternary (positional) :r 3)
(defnumsys quarternary (positional) :r 4)
(defnumsys quinary (positional) :r 5)
(defnumsys senary (positional) :r 6)
(defnumsys septenary (positional) :r 7)
(defnumsys octal (positional) :r 8)
(defnumsys nonary (positional) :r 9)
(defnumsys decimal (positional) :r 10)
(defnumsys undecimal (positional) :r 11)
(defnumsys duodecimal (positional) :r 12)
(defnumsys tridecimal (positional) :r 13)
(defnumsys tetradecimal (positional) :r 14)
(defnumsys pentadecimal (positional) :r 15)
(defnumsys hexadecimal (positional) :r 16)

(defnumsys vigesimal (positional) :r 20)
(defnumsys sexagesimal (positional) :r 60)

; complex integer numeral systems:
(defnumsys dragon (positional)
	:r #c(-1 1)
	:rules (list
	       	'(NIL ((0) (0)))
	       	'(+ ((0 -2) (-2 1) (-3 1)))))


(defnumsys quaterimaginary (positional)
	:r #c(0 2)
	:digits (cons #2<(#\. nil) '((#\3 3) (#\2 2) (#\1 1) (#\0 0)))
	:rules (list
	       	'(NIL ((0) (0)))
	       	'(+ ((-4 1) (-2 3) (0 -4)))))

;; Add custom ruleset for this complex irrational radix
;; as the generate-rules function can't handle complex values
(defnumsys rus-binary (positional)
	:r (/ (+ -1 (sqrt -7)) 2)
	:rules (list
	       	'(NIL ((0) (0)))
	       	'(+ ((0 -2) (-1 1) (-3 1)))))

(defnumsys negarootbinary (positional)
	:r (complex 0 (sqrt 2))
	:rules (list
	       	'(NIL ((0) (0)))
	       	'(+ ((-4 1) (-2 1) (0 -2)))))

;; add custom rulesset for this negative radix
;; as the generate-rules function can't handle negative values
(defnumsys negabinary (binary)
	:r -2
	:rules (list
	       	'(NIL ((0) (0)))
	       	'(+ ((0 -2) (-1 1) (-2 1)))))

(defnumsys balanced (ternary)
	:digits (cons #2<(#\. nil) '((#\T -1) (#\0 0) (#\1 1)))
	:rules (list
	       	'(NIL ((0) (0)))
	       	'(+ ((0 -3) (-1 1)))))


;; --- positional systems currently without working rulesets --- ;;

(defnumsys min-binary (binary)
	:digits '((#\1 1) (#\2 2)))

(defnumsys abcd (negabinary)
	:digits '((#\0 0) (#\A 1) (#\B -1) (#\C #c(0 1)) (#\D #c(1 1))))

;; --------------------------------------------------------- ;;

;; Define a generalised positional numeral system for
;; irrational numbers that uses the 'computational-reals'
;; which is a CL library for working with arbitrary precision.
(defnumsys positional-real ()
	:structure (sum-series-real +s+ term)
	:digits (radix-alphabet r)
	:term (*r (expt-r r n) +d+)
	:r 0
	
	:rules (generate-rules r))

;; Irrational radix systems using the more precise computational-reals
(defnumsys rootbinary (positional-real) :r (sqrt-r 2))
(defnumsys phinary (positional-real) :r (/r (+r 1 (sqrt-r 5)) 2))
(defnumsys silverbase (positional-real) :r (+r 1 (sqrt-r 2)))

(defvar conj+ (/r (+r 9 (expt-r 69 (/r 1 2))) 18))
(defvar conj- (/r (-r 9 (expt-r 69 (/r 1 2))) 18))
(defvar plastic (+r (expt-r conj+ (/r 1 3)) (expt-r conj- (/r 1 3))))
(defnumsys plasticbinary (positional-real) :r plastic)

;; --------------------------------------------------------- ;;

;; Here is a non-positional system for comparison
(defnumsys roman ()
	:structure (loop for d across +s+ sum term)
	:digits '((#\I 1) (#\V 5) (#\X 10) (#\L 50) (#\C 100) (#\D 500) (#\M 1000))
	:term +d+)

