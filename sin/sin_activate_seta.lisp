
(defvar *sin-active-systems* nil)

(defun activate (&rest systems)
	(setq *sin-active-systems* systems))

(defun match-system (value hardtype)
	(if (null hardtype)
		(loop for typ in *sin-active-systems* do
			(if (typep value typ)
				(return typ)))
		(if (typep value hardtype)
			hardtype)))

(defmacro seta (name value &aux systype)
	(if (and (boundp name) (typep (eval name) 'numsys))
		(let ((current-system (numsys-value-system (eval name))))
			(if (typep value current-system)
				(setf (numsys-value-string (eval name)) value)))
		(let ((system (match-system (eval value) (eval systype))))
			(if (not (null system))
				(let ((numsys (make-numsys
				            :value-string value
				            :value-system system)))
					`(setq ,name ,numsys))))))

(defun seta* (qname value &aux qsystype)
	(if (and (boundp qname) (typep (eval qname) 'numsys))
		(let ((current-system (numsys-value-system (eval qname))))
			(if (typep value current-system)
				(setf (numsys-value-string (eval qname)) value)))
		(let ((system (match-system (eval value) (eval qsystype))))
			(declare (special qname))
			(if (not (null system))
				(let ((numsys (make-numsys
				            :value-string value
				            :value-system system)))
					(format t "Setting '~a' to '~a'~%" qname numsys)
					(set qname numsys)))
			(format t "hmmm: ~a~%" (eval qname)))))

;; simple macro for looping through a list of number systems
(defmacro loop-active-systems (systems &rest body)
	`(loop for +activesystem+ in ,systems do (progn (activate +activesystem+) ,@body)))

;; macro for looping through a list of lists of number systems
(defmacro loop-active-systems-multi (systems &rest body)
	`(loop for +activesystems+ in ,systems do (progn (activate (values-list +activesystems+)) ,@body)))
