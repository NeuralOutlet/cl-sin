
; structure: the code used to traverse the digits of the number.
; digits: the mapping of character to value of each digit.
; term: what to interpret each digit as.
;
; inbuilt variables:
;    +n+    The position of the current digit
;    +s+    The set of all symbols (digits/numerals)
;    +d+    The current digit (char->int map from digits)


; ------------------
; internal functions
; ------------------

(defun numsys-basicp (sym)
	(cond
		((equal (symbol-name sym) "STRUCTURE") t)
		((equal (symbol-name sym) "DIGITS") t)
		((equal (symbol-name sym) "RULES") t)
		((equal (symbol-name sym) "TERM") t)))

(defun parse-key-list (lst)
	(loop for i from 0 to (length lst) by 2 when
		(not (numsys-basicp (nth i lst))) collect
		(list (find-symbol (symbol-name (nth i lst))) (nth (1+ i) lst)) while
			(< i (- (length lst) 2))))

(defun add-to-symbol (name suffix)
	"Create a symbol using a symbol and a string for the suffix"
	(intern (concatenate 'string (symbol-name name) suffix)))

(defun illegal-count (ch op lim str) ; character operator limit
	(if (null lim)
		nil
		(if (not (funcall op (count ch str) lim))
			t)))


; --------------------------------
; Define Numeral System: Typeclass
; --------------------------------


;; really basic container for systems
(deftype nilstring () '(or string null))
(defstruct numsys
	(value-string nil :type nilstring)
	(value-system nil :type symbol))

;; Declare Numeral System
(defmacro decnumsys (value-system)
	(make-numsys :value-system value-system))


;; Define Numeral System
(defmacro defnumsys (name supertype &rest user-variables
                     &key structure digits term rules &allow-other-keys)
	; Derrive from supertype
	(if (null structure)
		(setq structure (get (car supertype) 'structure)))
	(if (null digits)
		(setq digits (get (car supertype) 'digits)))
	(if (null term)
		(setq term (get (car supertype) 'term)))
	(if (null rules)
		(setq rules (get (car supertype) 'rules)))

	; Process the main components (structure/term/digits)
	(setq term (cond
		((listp term) (loop for item in term collect
		              	(if (equal item '+D+) '(cadr (assoc d digits)) item)))
		((equal term '+D+) '(cadr (assoc d digits)))
		(t term)))

	(setq structure (loop for item in structure collect
		(if (equal item 'term) term item)))

	;; deal with user defined key pair variables and subtyping of them
	;; here we parse the keypairs to get only user ones
	;; then merge them with any supertype user key pairs (we want to
	;; overwrite supertype keypairs with this type's keypairs)
	(let ((uservars (parse-key-list user-variables))
	      (super-uservars (get (car supertype) 'uservars))
		  (merged-uservars '()))

		(setq merged-uservars
			(loop for pair in super-uservars collect
				(let ((redefined (assoc (car pair) uservars)))
					(if (null redefined)
						pair
						redefined))))
	
		(setq merged-uservars (append merged-uservars
			(loop for pair in uservars
			  when (null (assoc (car pair) merged-uservars))
			    collect pair)))
	
		(setf (get name 'uservars) merged-uservars))

	;; this is the section engineering all the numsys type code
	;; the let-builder is where we get out local variables +n+ and +s+
	;; and is part of each functions definition in this section so they
	;; all have the same scope.
	(let* ((let-builder (append
	                    	'((+n+ (length numerals)) (+s+ numerals))
	                    	(get name 'uservars))))
		
		(eval `(defvar ,name (quote ,name)))
		(setf (get name 'structure) structure)
		(setf (get name 'digits) digits)
		(setf (get name 'term) term)
		(setf (get name 'type) 'system)
		(setf (get name 'rules) rules)

		(setf (get name 'digitmap)
			(eval `(let* ,(get name 'uservars)
				(loop for pair in ,digits when (not (null (cadr pair))) collect
					(cons (car pair) (cadr pair))))))

		; When constructing the following we need to make sure
		; user defined variables have been expanded properly
		(eval `(let* ,(get name 'uservars)

		        	; Generate the evaluation function
		        	(defun ,name (numerals &aux (digits ,digits))
		        		(let ,let-builder
		        			,structure))
		
		        	;; Generate a function to return (or compute) the rule set
		        	(defun ,(add-to-symbol name "-RULES") (&aux numerals digits)
		        		(let ,let-builder
		        			,rules))
		
		        	;; Generate a function to return (or compute) the alphabet
		        	(defun ,(add-to-symbol name "-ALPHABET") (&aux numerals digits)
		        		(let ,let-builder
		        			,digits))
		
		        	; mapped function for retrieving symbol values
		        	(defun ,(add-to-symbol name "-NUMERAL") (numeral)
		        		(if (characterp numeral)
		        			(cdr (assoc numeral (get ,name 'digitmap)))
		        			(car (rassoc numeral (get ,name 'digitmap)))))
		
		        	; Generate a type predicate that validates the language used
		        	(defun ,(add-to-symbol name "P") (var &aux (meta-digits ,digits)
		        	       (digits (loop for a in ,digits collect (car a))) (result t))
		        		(loop for ch across var do
		        			(progn
		        				(setq result (member ch digits))
		        				(if (not result)
		        					(return))))
		        		(if result
		        			(loop for md in meta-digits do
		        				(progn
		        					(setq result (not (illegal-count
		        						(nth 0 md) (find-symbol (string (nth 2 md))) (nth 3 md) var)))
		        					(if (not result)
		        						(return)))))
		        		(if result t))))
		
		; return the type
		`(deftype ,name () '(and string (satisfies ,(add-to-symbol name "P"))))))

; --------------------------
; Reader macro for alphabets
; --------------------------

;; Macro generator for putting character limits on alphabets
(defmacro make-dispatcher (some-op)
	`(set-dispatch-macro-character #\# ,some-op
		#'(lambda (s c n)
			(let ((lst (read s nil (values) t)))
				(when (consp lst)
					(list 'quote (append lst (list c n))))))))

; #3=(#\, nil)  means there must be exactly 3 commas
(make-dispatcher #\=)

; #10>(#\0 0)   means there must at least 10 zeros in the string
(make-dispatcher #\>)

; #2<(#\A 5)   means there must be fewer than 2 'A's in the string
(make-dispatcher #\<)


