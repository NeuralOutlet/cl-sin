
# System Interpreted Numbers (SIN) #

This is a language extension and collection of accompanying functions for defining and using custom numeral systems. It is currently focused mainly on positional numeral systems that follow a somewhat strict definition, though it can be used for others.

### (defnumsys) ###

This is the key function used to generate the type/typeclass for a system. It is a macro that takes three required parameters: structure, term, and digits which are used to define an evaluation function and a typecheck function. The user can use and define their own variables too which may be used inside the structure and term code.

Calling defnumsys will also define the following functions and symbol bindings:


```lisp
    (defnumsys mysystem ...)
        mysystem                     ; the symbol 'mysystem's value cell contains the symbol 'mysystem'
                                     ; the property list is filled with structure/term/digits/rules for use in subtyping

        mysystem                     ; a type specifier is bound creating the type 'mysystem'

        (mysystem <string>)          ; evaluation function converting from mysystem to decimal value
                                     ; evaluates the structure code and returns the result

        (mysystemp <string>)         ; type check function to see if <string> is a valid mysystem number

        (mysystem-alphabet <string>) ; evaluates the digits code and returns the result

        (mysystem-rules <string>)    ; evaluates the rules code and returns the result
```        


For example if we were to define Roman Numerals we could call defnumsys as such:


```lisp
    (defnumsys roman ()
        :structure (loop for d across +s+ sum term)
        :digits '((#\I 1) (#\V 5) (#\X 10) (#\L 50) (#\C 100) (#\D 500) (#\M 1000))
        :term +d+)
```

Which will generate the symbol roman, a type specifier 'roman', and the functions: roman, romanp roman-alphabet, and roman-rules (although the last will not work as the :rules argument was not given in the definition). This will allow the user to write things like (roman "XVI") which will return 16 and (romanp "XVI123") which would return false.

### Inductive Operators ###

Generate substitution laws that together well define euclidean arithmetic:

* Using beta-expansion to generate a standardisation law
* Using simple induction with the greedy/logmax algorithm to generate addition law


### Active Systems ###

This is for allowing a 'smart' dynamic type assignment. The user can set one or multiple systems, then any variables set with the active setters will type-check the symbols and assign them the first type that satisfies for the symbols.

    (activate <systems...>)
    (defvara <name> <string of symbols>)
    (seta <name> <string of symbols>)



